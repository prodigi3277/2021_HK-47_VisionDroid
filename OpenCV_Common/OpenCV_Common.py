import cv2 as cv
import numpy as np
import random
import CommonUtilities as CommUtils
# This library is only available on the Raspberry Pi.  Woes of cross platform development.
try:
	import RPi.GPIO as gpio
	raspberryEnv = True
except (ImportError, RuntimeError):
	raspberryEnv = False


class CommonOpenCVProcesses:
	'''
	Common processes and techniques that are useful when processing with OpenCV.
	'''
	commUtils = CommUtils.CommonUtilities()

	videoWriter = None

	drawOnOutput = False

	def __init__(self, gpioLedPin=18):
		self.gpioLedPin = gpioLedPin
		if raspberryEnv:
			gpio.setmode(gpio.BCM)
			gpio.setup(gpioLedPin, gpio.OUT)
			self.drawOnOutput = False  # Override to prevent accidental debug behavior on the Pi

	def initVideoWriter(self):
		videoWriterPath = "/media/RobotVision/videoCapture.mp4"
		self.commUtils.assurePathExists("/media/RobotVision/")
		videoWriterFourcc = cv.VideoWriter_fourcc('M', 'J', 'P', 'G')

		if raspberryEnv:
			videoWriterFourcc = cv.VideoWriter_fourcc('X', '2', '6', '4')

		self.videoWriter = cv.VideoWriter(videoWriterPath, videoWriterFourcc, 20.0, (640, 480))

	def medianBlurImage(self, argOriginalImage, argBlurSize=3):
		# Commonly seen as step 1 in processing
		# This is highly effective against salt-and-pepper noise in an image.
		# Add blur to the image to clear up noise.  Preserves edges.
		# argBlurSize Must be an odd number and > 1
		argBlurSize = argBlurSize - 1 if argBlurSize % 2 == 0 else argBlurSize
		return cv.medianBlur(argOriginalImage, argBlurSize)

	def gaussianBlurImage(self, argOriginalImage, argBlurSize=3):
		# Commonly seen as step 1 in processing
		# Gaussian blurring is highly effective in removing Gaussian noise from an image.
		# Add blur to the image to clear up noise.  Preserves edges.
		# argBlurSize Must be an odd number and > 1
		argBlurSize = argBlurSize - 1 if argBlurSize % 2 == 0 else argBlurSize
		return cv.gaussianBlur(argOriginalImage, (argBlurSize, argBlurSize), 2, 2)

	def getDesiredColorImageTwoRange(self, argOriginalImage, argLowerBGR_ColorRangeTuple, argUpperBGR_ColorRangeTuple):
		'''
		Get the desired color image given two BGR color ranges.  Expected input
		to be a tuple of tuples ((b,g,r),(b,g,r)) where left is lower and right is upper.

		Two ranges are sometimes desirable for situations where the color space varies due to glare
		or large changes in lighting when traversing across the field.
		'''
		lowT = argLowerBGR_ColorRangeTuple
		highT = argUpperBGR_ColorRangeTuple

		blurredImage = self.medianBlurImage(argOriginalImage)
		# Convert input image to HSV
		hsvImage = cv.cvtColor(blurredImage, cv.COLOR_BGR2HSV)

		'''
		Threshold the HSV image, keep only the colored pixels of interest
		Use Photoshop, Gimp, or gColor2 (or similar) to identify the HSV values in the 0-255 range.
		'''
		minLowerBGR = np.array([lowT[0][0], lowT[0][1], lowT[0][2]])
		maxLowerBGR = np.array([lowT[1][0], lowT[1][1], lowT[1][2]])

		lowerColorHueRange = cv.inRange(hsvImage, minLowerBGR, maxLowerBGR)

		minUpperBGR = np.array([highT[0][0], highT[0][1], highT[0][2]])
		maxUpperBGR = np.array([highT[1][0], highT[1][1], highT[1][2]])

		upperColorHueRange = cv.inRange(hsvImage, minUpperBGR, maxUpperBGR)

		# Combine the above two images
		desiredHueImage = cv.addWeighted(lowerColorHueRange, 1.0, upperColorHueRange, 1.0, 0.0)

		# Slightly blur the result, in order to avoid false positives
		return cv.GaussianBlur(desiredHueImage, (9, 9), 2, 2)

	def getDesiredColorImageSingleRange(self, argOriginalImage, argBGR_ColorRangeTuple):
		'''
		Get the desired color image given a BGR color range.  Expected input
		to be a tuple of tuples ((b,g,r),(b,g,r)) where left is lower and right is upper.

		Same as the double range version, but for single a single color sample range.  This may be
		useful where large changes in the color space is not expected... like in a lab or when a
		supplied light guarantees continuous expected results.
		'''
		crT = argBGR_ColorRangeTuple

		blurredImage = self.medianBlurImage(argOriginalImage)
		# Convert input image to HSV
		hsvImage = cv.Color(blurredImage, cv.COLOR_BGR2HSV)

		'''
		Threshold the HSV image, keep only the colored pixels of interest
		Use Photoshop, Gimp, or gColor2 (or similar) to identify the HSV values in the 0-255 range.
		'''
		minLowerBGR = np.array([crT[0][0], crT[0][1], crT[0][2]])
		maxLowerBGR = np.array([crT[1][0], crT[1][1], crT[1][2]])

		colorHueRange = cv.inRange(hsvImage, minLowerBGR, maxLowerBGR)

		# Slightly blur the result, in order to avoid false positives
		return cv.GaussianBlur(colorHueRange, (9, 9), 2, 2)

	def resizeWindow(self, argImage, argDesiredResolution=[1280, 720]):
		scaleWidth = argDesiredResolution[0] / argImage.shape[1]
		scaleHeight = argDesiredResolution[1] / argImage.shape[0]
		scale = min(scaleWidth, scaleHeight)

		# resized window width and height
		windowWidth = int(argImage.shape[1] * scale)
		windowHeight = int(argImage.shape[0] * scale)

		return windowWidth, windowHeight

	def captureReflectiveTapeImageDiff(self, argThreadedCameraCapture):
		'''
		This process is disruptive and should be conducted using the asyncio library
		or rewritten to be a separate thread.  The camera capture passed in is assumed
		to be threaded so this shouldn't hold things up if careful.
		'''
		reflectiveTapeImage = None
		if raspberryEnv:
			gpio.output(self.gpioLedPin, gpio.HIGH)
			pass

		return reflectiveTapeImage

	def getUsbVideoCaptureDimensions(self):
		'''
		Opens and closes a camera connection to identify the camera's native dimensions.
		These values should be used in the configuration of the camera and then used throughout
		the rest of the program as a known.
		'''
		videoCapture = cv.VideoCapture(0)
		videoCaptureWidth = videoCapture.get(cv.CAP_PROP_FRAME_WIDTH)
		videoCaptureHeight = videoCapture.get(cv.CAP_PROP_FRAME_HEIGHT)
		videoCapture.release()

		print('Camera Dimensions: Width - {}, Height - {}'.format(videoCaptureWidth, videoCaptureHeight))

		return int(videoCaptureWidth), int(videoCaptureHeight)

	def captureVideoToDisk(self, argFrame):
		if(self.videoWriter is None):
			self.initVideoWriter()
		else:
			self.videoWriter.write(argFrame)

	def identifyCircle(self, argFrame, argMinRadius=450, argMaxRadius=475):
		# This is a heavyweight function... be careful!
		# https://dsp.stackexchange.com/questions/22648/in-opecv-function-hough-circles-how-does-parameter-1-and-2-affect-circle-detecti
		gray = cv.cvtColor(argFrame, cv.COLOR_BGR2GRAY)

		gray = self.medianBlurImage(gray)

		rows = gray.shape[0]
		circles = cv.HoughCircles(gray, cv.HOUGH_GRADIENT, 1, rows / 8, param1=100, param2=30, minRadius=argMinRadius, maxRadius=argMaxRadius)

		if self.drawOnOutput:
			if circles is not None:
				circles = np.uint16(np.around(circles))
				for i in circles[0, :]:
					center = (i[0], i[1])
					randColor = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
					# circle center
					cv.circle(argFrame, center, 1, randColor, 3)
					# circle outline
					radius = i[2]
					cv.circle(argFrame, center, radius, randColor, 3)

					windowName = 'Detected Circles'

					# cv.WINDOW_NORMAL makes the output window resizable
					cv.namedWindow(windowName, cv.WINDOW_NORMAL)

					# resize the window according to the screen resolution
					cv.resizeWindow(windowName, self.resizeWindow(argFrame))

				cv.imshow(windowName, argFrame)
				cv.waitKey(0)

	def __exit__(self, exec_type, exc_value, traceback):
		if raspberryEnv:
			gpio.cleanup()


class CommonOpenCVProcessesTest:
	'''
	Run all the tests here
	'''
	commonOpenCVProcesses = None
	frame = None
	imagePath = None

	def __init__(self):
		# Please avoid committing the test file name
		self.imagePath = r'C:\Path\To\Image\Here\file.jpg'

		self.frame = cv.imread(self.imagePath)
		self.commonOpenCVProcesses = CommonOpenCVProcesses()

	def commonOpenCVProcessesTestCircle(self):
		self.commonOpenCVProcesses.identifyCircle(self.frame)


def main():
	debugTesting = False
	if(debugTesting):
		ct = CommonOpenCVProcessesTest()
		ct.commonOpenCVProcessesTestCircle()


if __name__ == '__main__':
	main()
