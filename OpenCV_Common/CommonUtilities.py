import os
import pathlib
# This library is only available on the Raspberry Pi.  Woes of cross platform development.
try:
	import RPi.GPIO as gpio
	raspberryEnv = True
except (ImportError, RuntimeError):
	raspberryEnv = False

class CommonUtilities:
	'''
	Common processes that are not OpenCV specific.
	'''

	def __init__(self):
		pass

	def assurePathExists(self, argPath):
		dir = os.path.dirname(f'{argPath}')
		if not os.path.exists(dir):
			os.makedirs(dir)

	def isJpegFile(argFullFilePath=r'C:\DoesNotExist.dne'):
		isJpegFile = False
		path = pathlib.Path(argFullFilePath)
		if ".jpg" == path.suffix:
			isJpegFile = True

		return isJpegFile

	def isVideoFile(argFullFilePath=r'C:\DoesNotExist.dne'):
		isVideoFile = False
		videoContainer = ''

		if raspberryEnv:
			videoContainer = ".mp4"
		else:
			videoContainer = ".avi"

		path = pathlib.Path(argFullFilePath)
		if videoContainer == path.suffix:
			isVideoFile = True

		return isVideoFile

	def rescale(self, argPreviousValue, argOldRangeMax, argOldRangeMin=0, argNewRangeMin=0, argNewRangeMax=1000.0):
		'''
		The camera has a specific width and height.  This provides a means to
		re-scale the value to conform to another range necessary for further processing.
		argPreviousValue is the input value needed to rescale
		argOldRangeMax typically is the max camera height or width.  May also be another reasonable value to rescale against
		argOldRangeMin is the lowest value of the old scale
		argNewRangeMin is the lowest value of the new scale
		argNewRangeMax is the largest value of the new scale
		Thanks to http://stackoverflow.com/questions/5294955/how-to-scale-down-a-range-of-numbers-with-a-known-min-and-max-value
		'''

		return (((argNewRangeMax - argNewRangeMin) * (argPreviousValue - argOldRangeMin)) / (argOldRangeMax - argOldRangeMin)) + argNewRangeMin
